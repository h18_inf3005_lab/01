#Definir une classe.
#Voir la doc ici : https://docs.python.org/3/tutorial/classes.html#class-objects
#La différence entre une instance de classe et une variable de classe est importante
#Voir à l'adresse ci-dessous
#https://www.python-course.eu/python3_class_and_instance_attributes.php
class Client:
	#init 
	def __init__(self, nom, client_num):
		self.client_num = client_num
		self.nom = nom
	#Je suis toujours dans la classe

#Je suis sorti de la classe car je suis revenu à l'indentation de départ

#Utilisation de la classe.
client1 = Client('fred', 1)

#Ouvrir et manipuler un fichier
fichier_entree = open('entree.txt', 'r')
fichier_sortie = open('nomdufichierdesortie.txt', 'w')

#Une boucle for simple en Python.
for line in fichier_entree:
	print (line)
	
#Une boucle for avec index en Python
for idx, line in enumerate(fichier_entree):
	print ('index de la ligne', idx)

#Comme on vous l'a appris en Prog2, il est important de fermer les fichiers.
fichier_entree.close()
fichier_sortie.close()

#Il existe une autre manière d'ouvrir des fichiers en Python qui gère la fermeture tout seule :
#https://openclassrooms.com/courses/apprenez-a-programmer-en-python/les-fichiers-2#/id/r-2232732

#Definir une fonction. Encore une fois, regardez la doc Python pour vous donner une meilleure idée !
def function1(param1, param2):
	produit = 'test'
	
	return produit