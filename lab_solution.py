# -*- coding: utf-8 -*-

# Création d'une classe Client
class Client:
	def __init__(self, client_num):
		self.client_num = client_num
		self.total_brut = 0
		self.rabais = self.total_brut * 0.15
		self.total = self.total_brut-self.rabais
		self.produits = []

#Création d'une fonction qui explose en colonnes une ligne du texte d'entrée
#Chaque ligne est transformée en un produit, sous forme de dictionnaire
#Cette fonction retourne un dictionnaire par ligne
#Elle n'est pas efficace, notamment en terme de nettoyage de données
#TODO : Rendre les données plus facile à utiliser plus bas
def ajout_dico(f_entree, line):
	produits = {}
	split_lines = line.split()
	produits['client_num'] = split_lines[0]
	produits['prod_num'] = split_lines[1]
	produits['prod_qte'] = split_lines[2]
	produits['prix_unit'] = split_lines[3]
	if len(split_lines) > 4:
		produits['taxes'] = split_lines[4]
	else:
		produits['taxes'] = 0
	return produits

#fonction principale
#Elle n'est pas efficace, comment l'amélioreriez-vous ?
#TODO: Puis-je enlever une boucle dans celle-ci ?
#TODO: Comment puis-je la rendre plus lisible ?
def main():
	#Ouvrir le fichier principal
	#La fonction open seule n'est pas toujours la meilleure en Python
	#Qu'utiliseriez-vous à la place ?
	#Hint: https://docs.python.org/3/tutorial/inputoutput.html#reading-and-writing-files
	f_entree = open('entree.txt', 'r')
	liste_produits = []
		
	clients = []
	for line in f_entree:
		liste_produits.append(ajout_dico(f_entree, line))

	#On ferme le fichier et sa variable de type fichier associée
	f_entree.close

	# Cette boucle itère à travers la liste des produits, un array de dictionnaires
	# Chaque produit doit être associé à un client, qui possède aussi un array de produits
	# Cette boucle ne fonctionne pas correctement, pourquoi?
	# TODO: Corriger le test des produits
	for idx, produit in enumerate(liste_produits):
		if idx == 0:
			client = Client(produit['client_num'])
			client.produits.append(produit)
		else:
			if client.client_num == produit['client_num']:
				client.produits.append(produit)
			else:
				clients.append(client)
				client = Client(produit['client_num'])
				client.produits.append(produit)

	#Cette boucle itère à travers l'array de clients afin d'écrire correctement chacune des factures
	# Il semble y avoir des problèmes d'affichage, comment les régler?
	#Astuce : Python 3 a des string formatting. Voyez la documentation des string formatting operators
	for client in clients:
		f_sortie = open(client.client_num+'.txt', 'w')
		f_sortie.write('Client numéro ' + client.client_num+'\n\n')
		f_sortie.write('No de produit \t\t\tQte Prix \t Total (tx)'+'\n')
		qte_counter = 0
		total = 0
		for produit in client.produits:
			total_produit = int(produit['prod_qte'])*float(produit['prix_unit'])
			qte_counter += qte_counter + int(produit['prod_qte'])
			f_sortie.write('Produit #'+str(idx)+'\t'+produit['prod_num']+'\t'+produit['prod_qte']+'\t'+produit['prix_unit']+'\t'+str(total_produit)+'\n')
			if produit['taxes'] != 0:
				if produit['taxes'] == 'FP':
					total_produit += total_produit*0.15
				elif produit['taxes'] == 'F':
					total_produit += total_produit*0.07
				else:
					total_produit += total_produit*0.08

			total += total_produit

		f_sortie.write('\nTotal: '+str(total)+'\n')
		rabais = total * 0.15
		f_sortie.write('Rabais : '+str(rabais)+'\nTotal :'+str(total-rabais) if qte_counter > 100 else 'Rabais : ' +str(0)+'\nTotal : '+str(total)+'\n')
		f_sortie.close


main()