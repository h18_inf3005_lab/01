# -*- coding: utf-8 -*-
#Permet de définir le mode utf8 afin que python puisse gérer les caractères accentués

#!/usr/bin/python3
#shebang line. Définit où le compilateur est sensé se trouver.
#Pour plus d'informations à son sujet : https://stackoverflow.com/questions/7670303/purpose-of-usr-bin-python3

#Print, il faut concaténer des strings pour qu'elles sortent correctement
#C'est pour ça que l'on cast le calcul en string
print ('Produit' + str(33 + 33))

haha = 0
#Un autre type de print utilisant le string formatting operator
#Plus d'informations ici: http://python-reference.readthedocs.io/en/latest/docs/str/formatting.html
print ('variable haha %i', haha)

#conditions de base avec if. Allez voir la doc pour les opérateurs
if haha == 0:
	print ('haha')
elif haha != 0:
	print ('hihi')
else:
	print ('hoho')

#Arrays
produits = []
#Ajouter à la fin d'un array. La doc est encore une fois votre amie pour les méthodes pertinentes rattachées aux arrays.
produits.append(5)
print (produits)
	
#Dictionnaire
produits = {}
#Ajouter une cle et une valeur a un dictionnaire
produits['cle'] = 1
#Acceder a une cle en particulier d'un dictionnaire
print (produits['cle'])
#La doc est encore une fois votre amie.