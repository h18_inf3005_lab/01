#Étapes possibles pour ce lab

### Installer Python3, si vous utilisez une machine personnelle. Bien cocher Add to PATH sur Windows
### Ajouter une ligne permettant d'utiliser UTF8. Google est votre ami
### Rappels sur Python et les indentations
### Apprendre à utiliser les conditions: if, elif, else
### Utiliser les boucles for et while
### Utiliser les boucles for et while avec index. Enumerate.
### Créer des arrays et dictionnaires et les populer
### Définir des fonctions
### Définir des classes
### Différence entre variable de classe et variable d'instance